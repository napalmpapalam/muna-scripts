module.exports = {
  root: true,
  extends: [
    'eslint-config-standard',
  ],
  env: {
    node: true,
    browser: true,
  },
  plugins: ['promise', 'import', 'node', 'vue'],
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'module',
  },
  rules: {
    'arrow-parens': 0,
    'generator-star-spacing': 0,
    'no-debugger': 1,
    'no-warning-comments': [1, {
      terms: ['hardcoded'], location: 'anywhere',
    }],
    'no-tabs': 2,
    'max-len': [1, {
      code: 80,
      comments: 80,
      ignoreUrls: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
      ignoreRegExpLiterals: true,
    }],
    'no-console': [
      1,
      {
        allow: [
          'warn',
          'error',
          'log',
        ],
      },
    ],
    indent: 1,
    'comma-dangle': [1, 'always-multiline'],
    'linebreak-style': ['error', 'unix'],
    'object-curly-spacing': ['error', 'always'],
    'no-var': 'error',
    'promise/always-return': 'error',
    'promise/no-return-wrap': 'error',
    'promise/param-names': 'error',
    'promise/catch-or-return': 'error',
    'promise/no-nesting': 'warn',
    'promise/avoid-new': 'warn',
    'promise/no-callback-in-promise': 'warn',
    'promise/prefer-await-to-then': 'warn',
  },
}
