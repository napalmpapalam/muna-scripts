const { initApiWithMasterWallet, base, api, ENVIROMENTS } = require('../api')

const ROLE_CORPORATE_BUILDER_OPTS = {
  details: {},
  // ruleIDs: [
  //   '2',
  //   '3',
  //   '4',
  //   '5',
  //   '7',
  //   '8',
  //   '9',
  //   '10',
  //   '11',
  //   '14',
  //   '15',
  //   '16',
  //   '17',
  //   '18',
  //   '19',
  //   '20',
  //   '21',
  //   '22',
  //   '23',
  //   '24',
  //   '25',
  //   '26',
  //   '27',
  //   '28',
  //   '31',
  //   '32',
  //   '34',
  //   '35',
  //   '36',
  //   '37',
  //   '39',
  // ],
  ruleIDs: [
    '2',
    '3',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '12',
    '13',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '25',
    '26',
    '27',
    '28',
    '30',
    '31',
    '32',
    '33',
    '34',
    '35',
    '36',
    '38',
    '39',
  ],
}

const MASTER_KEYS = {
  aura: {
    staging: {
      public: 'GD4NW3M4JMPT5C7P3AWRB4CUI2GPYDTYI5O6MV3CMWJN2XZH3QLWUKAK',
      private: 'SBNJ3QFXWDJMGE3V4OCFRJQDICIABV6WWR6V5FTBBQ2CYSDQDG7CNAT4',
    },
    prod: {
      public: 'GA35MKVN4YP7X7OJMMKL3XBKPKMJ3GZYI44L5WOLRZHIXQG2AWRHKC4E',
      private: 'SDLYVAMXPN5X6ONAQW4BJVZLPNLJHYVADXUUFU4JHH2ZNIKIPL3BRC6E',
    },
  },
}

const createAccountRoleOp = opts => base.ManageAccountRoleBuilder.create(opts)

const addAccountRole = async ({
  masterPublicKey,
  masterSecretKey,
  env,
  roleOpts,
}) => {
  try {
    const wallet = await initApiWithMasterWallet({
      accountId: masterPublicKey,
      seed: masterSecretKey,
      env,
    })

    console.log('wallet', wallet)

    const op = createAccountRoleOp(roleOpts)

    const response = await api.postOperations(op)
    console.log('response', response)
  } catch (e) {
    console.log(e)
  }
}

addAccountRole({
  masterPublicKey: MASTER_KEYS.aura.staging.public,
  masterSecretKey: MASTER_KEYS.aura.staging.private,
  env: ENVIROMENTS.stagingAura,
  roleOpts: ROLE_CORPORATE_BUILDER_OPTS,
})
