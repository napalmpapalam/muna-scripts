const { initApiWithWallet, api } = require('../api')

// const ROLES = { hospital: 'hospital', patient: 'patient', verifier: 'verifier' }

const API_PARAMS = {
  env: 'prod', // local | stage | prod
  email: 'doctor@email.com',
  password: '122134',
}

async function getChangeRoleRequests () {
  const limit = 1
  const order = 'desc'

  const wallet = await initApiWithWallet(API_PARAMS)

  const response = await api.getWithSignature('/v3/change_role_requests', {
    filter: {
      'request_details.destination_account': wallet.accountId,
    },
    page: { limit, order },
    include: ['request_details'],
  })

  console.log(response)
}

getChangeRoleRequests()
