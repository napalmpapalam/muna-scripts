const { initApiWithMasterWallet, base, api, ENVIROMENTS } = require('../api')

// keemoji keys
const MASTER_KEYS = {
  staging: {
    public: 'GBA4EX43M25UPV4WIE6RRMQOFTWXZZRIPFAI5VPY6Z2ZVVXVWZ6NEOOB',
    private: 'SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4',
  },
}

const createUpdateAssetOp = () => {
  return base.ManageAssetBuilder.assetUpdateRequest({
    requestID: '0',
    code: String(this.asset.id),
    policies: Number(this.asset.policies.value),
    allTasks: 0,
    creatorDetails: {
      name: this.asset.creatorDetails.name,
      external_system_type: this.asset
        .creatorDetails.externalSystemType,
      is_coinpayments: this.asset.creatorDetails.isCoinpayments,
      logo,
      terms,
      stellar: this.getStellarData(),
      erc20: this.getErc20Data(),
  })
}

const updateAsset = async ({
  masterPublicKey,
  masterSecretKey,
  env,
}) => {
  try {
    const wallet = await initApiWithMasterWallet({
      accountId: masterPublicKey,
      seed: masterSecretKey,
      env,
    })

    const op = createUpdateAssetOp()
    const response = await api.postOperations(op)
    console.log('response', response)

    console.log('wallet', wallet)
  } catch (e) {
    console.log(e)
  }
}

updateAsset({
  masterPublicKey: MASTER_KEYS.staging.public,
  masterSecretKey: MASTER_KEYS.staging.private,
  env: ENVIROMENTS.stagingKeemoji,
})
