const { ApiCaller, WalletsManager, base } = require('@tokend/js-sdk')

const api = new ApiCaller()
const walletsManager = new WalletsManager()
const geocode = { latitude: 0, longitude: 0 }
const PATIENT_ACCOUNT_ROLE_KEY_VALUE = 5

async function initApi () {
  /** When you testing some functionality, do it, please on staging
   * horizon server urls:
   * staging: 'https://api.staging.munahealth.com'
   * production: 'https://api.munahealth.com'
   */
  api.useBaseURL('https://api.staging.munahealth.com')
  const { data: networkDetails } = await api.getRaw('/')
  api.useNetworkDetails(networkDetails)
  walletsManager.useApi(api)
}

async function createIdentity (email, passport) {
  let identity
  try {
    const { data } = await api.post('/identities', {
      data: { type: 'identity', attributes: { email, passport } },
    })

    identity = data
  } catch (error) {
    console.log(error)
  }

  console.log('GOT IDENTITY:', identity)
  return identity
}

async function getLaboratoryWallet () {
  const wallet = await walletsManager.get(
    'laboratory@email.com',
    'password',
    geocode,
  )

  return wallet
}

async function createKycBlob (laboratoryAccountId, kyc, blobType = 'kyc_form') {
  try {
    const { data: blob } = await api.postWithSignature('/blobs', {
      data: {
        type: blobType,
        attributes: { value: JSON.stringify(kyc) },
        relationships: {
          owner: { data: { id: laboratoryAccountId } },
        },
      },
    })

    console.log('GOT BLOB:', blob)
    return blob.id
  } catch (error) {
    console.log(error)
  }
}

function createKycOperation (kycBlobId, patientAccountId, accountRole) {
  return base.CreateChangeRoleRequestBuilder.createChangeRoleRequest({
    requestID: '0', // KYC_CREATION_REQUEST_ID
    destinationAccount: patientAccountId,
    accountRoleToSet: String(accountRole),
    /**
       * this value you have to get on key values endpoint, it can be changed,
       * so if you want to be sure that you are using right value you have to
       * get "account_role:general" key value, same for type of operation,
       * when you submitting test result ("data_type:test_result").
       *
       * Now account role of patient on staging and prod. versions are different:
       * on staging it is 5, on prodution it is 6
       */
    creatorDetails: { blob_id: kycBlobId },
  })
}

function getPatientKyc () {
  return {
    first_name: 'John',
    last_name: 'Doe',
    birthday_date: new Date('1995-08-20').toISOString(), // RFC3339
    gender: 'Male', // 'Male' || 'Female' || 'Prefer not to say'
    phone_number: '+380991111111',
    address: {
      line1: '2648  My Drive',
      line2: '',
      postal_code: '10013',
    },
    city: 'New York',
    country: 'United States',
    documents: {
      kyc_avatar: {
        mime_type: '',
        name: '',
        key: '',
      },
    },
  }
}

async function createPatientKyc (patientAccountId, laboratoryAccountId, kyc) {
  try {
    const blobId = await createKycBlob(laboratoryAccountId, kyc)
    const operation = createKycOperation(
      blobId,
      patientAccountId,
      PATIENT_ACCOUNT_ROLE_KEY_VALUE,
    )

    await api.postOperations(operation)
  } catch (error) {
    console.log(error)
  }
}

async function createPatient () {
  try {
    await initApi()

    const { address: patientAccountId } = await createIdentity(
      /** this values must be unique, otherwise you will get 500 error */
      'patient@email.com', // lowercase
      'PASSPORTID', // uppercase
    )

    const wallet = await getLaboratoryWallet()
    api.useWallet(wallet)
    walletsManager.useApi(api)

    const laboratoryAccountId = wallet._accountId
    const patientKyc = getPatientKyc()

    await createPatientKyc(patientAccountId, laboratoryAccountId, patientKyc)
  } catch (error) {
    console.log(error)
  }
}

createPatient()
