const { initApiWithWallet, api } = require('../api')

async function getRootLabList () {
  try {
    await initApiWithWallet({
      env: 'local',
      email: 'doctor@email.com',
      password: '122134',
    })

    const { data } = await api.getWithSignature('/integrations/test-results/labs')
    console.log(data)
  } catch (error) {
    console.log(error)
  }
}

getRootLabList()
