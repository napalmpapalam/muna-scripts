const { ApiCaller, WalletsManager, base, Wallet } = require('@tokend/js-sdk')

const api = new ApiCaller()
const walletsManager = new WalletsManager()

const ADMIN_ACCOUNT_ID = 'GCHJ2BYEQEWLZSA6ZBOVWX5J7HXBOCPU4WWCAENYTHCFCNQVXFDYRPQX'
const ADMIN_SEED = 'SDTQ3OZEJPTJY3H62HOHVTE5G2IDATP7GOTT7Q3PGJ3NMSZWRYEZPW4M'

async function init () {
  try {
    api.useBaseURL('https://api.munahealth.com')
    const { data: networkDetails } = await api.getRaw('/')
    api.useNetworkDetails(networkDetails)

    const keyPair = base.Keypair.fromSecret(ADMIN_SEED)
    const seed = keyPair.secret()
    const wallet = new Wallet(
      '',
      seed,
      ADMIN_ACCOUNT_ID,
    )
    console.log(wallet)

    api.useWallet(wallet)
    walletsManager.useApi(api)
  } catch (error) {
    console.log(error)
  }
}

async function updateSigner () {
  try {
    await init()
    const op = createUpdateSignerOp()
    const response = await api.postOperations(op)
    console.log(response)
  } catch (error) {
    console.log(error)
  }
}

function createUpdateSignerOp () {
  const op = base.ManageSignerRoleBuilder.update({
    roleId: '9',
    details: {
      admin_role: '0',
      description: 'Role allows signer only to watch users list and verification history',
      name: 'Verifier admin',
    },
    ruleIDs: ['15', '18'],
    source: ADMIN_ACCOUNT_ID,
  })
  console.log('operation', op)
  return op
}

function createCreateSignerOp () {
  const op = base.ManageSignerRoleBuilder.create({
    details: {
      admin_role: '0',
      description: 'Check key value signer-role:admin_ligth to get "Admin Light" role ID',
      name: 'Admin Light',
    },
    ruleIDs: [],
    source: ADMIN_ACCOUNT_ID,
  })
  console.log('operation', op)
  return op
}

updateSigner()
