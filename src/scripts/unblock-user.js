const { initApiWithMasterWallet, createKycOperation, api } = require('../api')

const unblockUser = async _ => {
  try {
    await initApiWithMasterWallet({
      accountId: 'GCHJ2BYEQEWLZSA6ZBOVWX5J7HXBOCPU4WWCAENYTHCFCNQVXFDYRPQX',
      seed: 'SDTQ3OZEJPTJY3H62HOHVTE5G2IDATP7GOTT7Q3PGJ3NMSZWRYEZPW4M',
      env: 'prod',
    })

    const op = createKycOperation({
      kycBlobId: '7FSLCN6MYUDR65Y2AZLDA3GKRAOVF7GTW76JWMNNGEHAFN6TAKZQ',
      destinationAccountId: 'GAFYOKEKJJXG4UZESXYCBFPJHLJ6EOIYQ5G4TQZAGPVCW7NPCQWAZ2SE',
      accountRole: 2,
    })
    const response = await api.postOperations(op)
    console.log(response)
  } catch (e) { console.log(e) }
}

unblockUser()
