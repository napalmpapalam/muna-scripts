const { initApiWithWallet, base, api, BLOB_TYPES } = require('../api')

const ROLES = { hospital: 'hospital', patient: 'patient', verifier: 'verifier' }

// ***************************** CHANGE THIS **********************************

const API_PARAMS = {
  env: 'local', // local | stage | prod
  email: 'user@email.com',
  password: '122134',
}

const ROLE_ID = '6' // key value role id
const ROLE_TO_SET = ROLES.patient // hospital | patient | verifier
const KYC_REQUEST_ID = '0' // 0 - create kyc request or something else - update

// ****************************************************************************

const createChangeRoleRequest = async () => {
  try {
    const wallet = await initApiWithWallet(API_PARAMS)
    console.log('WALLET:', wallet)

    const kycBlob = createKyc(ROLE_TO_SET)
    if (!kycBlob) { throw new Error('ROLE TO SET setted not correctly') }
    console.log('KYC_BLOB:', kycBlob)

    const kycBlobId = await createKycBlob(wallet.accountId, kycBlob)
    const operation = createChangeRoleOperation(wallet.accountId, kycBlobId)
    console.log('OPERATION:', operation)

    const tx = await api.postOperations(operation)
    console.log('TX:', tx)
  } catch (e) {
    console.log('ERROR:', e)
  }
}

const createChangeRoleOperation = (destinationAccountId, blobId) => {
  return base.CreateChangeRoleRequestBuilder.createChangeRoleRequest({
    requestID: KYC_REQUEST_ID,
    destinationAccount: destinationAccountId,
    accountRoleToSet: `${ROLE_ID}`,
    creatorDetails: { blob_id: `${blobId}` },
  })
}

const createKycBlob = async (blobOwnerAccountId, blobValue) => {
  const { data: blob } = await api.postWithSignature('/blobs', {
    data: {
      type: ROLE_TO_SET === ROLES.hospital
        ? BLOB_TYPES.kycCorporate
        : BLOB_TYPES.kycGeneral,
      attributes: { value: JSON.stringify(blobValue) },
      relationships: { owner: { data: { id: blobOwnerAccountId } } },
    },
  })
  return blob.id
}

const createPatientKycData = () => {
  return {
    first_name: 'Semen',
    last_name: 'Doe',
    birthday_date: new Date('1995-08-20').toISOString(), // RFC3339
    gender: 'Male', // 'Male' || 'Female' || 'Prefer not to say'
    phone_number: '+380991111111',
    address: { line1: '2648  My Drive', line2: '', postal_code: '10013' },
    city: 'New York',
    country: 'United States',
    documents: {
      kyc_avatar: { mime_type: '', name: '', key: '' },
    },
  }
}

const createVerifierKycData = () => {
  return {
    documents: {
      additional_files: { key: '', mime_type: '', name: '' },
      kyc_avatar: { key: '', mime_type: '', name: '' },
    },
    first_name: 'John',
    last_name: 'Doe',
    birthday_date: new Date('1995-08-20').toISOString(), // RFC3339
    gender: 'Male', // 'Male' || 'Female' || 'Prefer not to say'
    phone_number: '+380991111111',
    qualification: 'qualification',
    registration_body: 'registration_body',
    registration_date: '1899-12-31T22:00:00.000Z',
    registration_number: 'registration_number',
    specialization: 'specialization',
    work_location: 'work_location',
    work_location_place: 'work_location_place',
  }
}

const createCorporateKycData = () => {
  return {
    hospital_name: 'hospital',
    phone_number: '+380991111111',
    pcr_test_lab_id: 'attrs.PCRTestLabId',
    hospital_website: 'http://google.com',
    pcr_test_country_iso_code: 'ISOCODE',
    registration_date: new Date().toISOString(),
    registration_body: 'registrationBody',
    hospital_country: 'Afghanistan',
    hospital_city: 'Herat',
    address: {
      line1: 'addressline1',
      line2: 'addressline1',
      postal_code: '51112',
    },
    registration_number: '12ASD',
    account_name: '',
    staff: [
      {
        degree_title: 'Dr.',
        first_name: 'John',
        last_name: 'Doe',
        birthday_date: new Date('1995-08-20').toISOString(),
        gender: 'Male',
        phone_number: '+380991111111',
        qualification: 'qualification',
        specialization: 'sdasdasd',
        testing_experience: 'asdasdasdas',
        country: 'Afghanistan',
        city: 'Herat',
        email: 'asdasd@asdasd.com',
        address: {
          line1: 'addressline1',
          line2: 'addressline1',
          postal_code: '51112',
        },
        documents: {
          additional_files: { licenseFile: { key: '', mime_type: '', name: '' } },
          kyc_avatar: { key: '', mime_type: '', name: '' },
        },
      },
    ],
    documents: {
      additional_files: {
        license: { key: '', mime_type: '', name: '' },
        policy: { key: '', mime_type: '', name: '' },
      },
      kyc_avatar: { key: '', mime_type: '', name: '' },
    },
  }
}

const createKyc = role => {
  switch (role) {
    case ROLES.hospital:
      return createCorporateKycData()
    case ROLES.patient:
      return createPatientKycData()
    case ROLES.verifier:
      return createVerifierKycData()
  }
}

createChangeRoleRequest()
