const { initApiWithWallet, walletsManager, factorsManager, base, api } = require('../api')

const keypairs = []

async function addSigner () {
  try {
    const wallet = await initApiWithWallet({
      env: 'local',
      email: 'doc2@email.com',
      password: '122134',
    })

    console.log(wallet)

    await walletsManager.changePassword('122134', [base.Keypair.random()])
  } catch (error) {
    try {
      await factorsManager.verifyPasswordFactorAndRetry(error, '122134')

      await initApiWithWallet({
        env: 'local',
        email: 'doc2@email.com',
        password: '122134',
      })

      const ops = keypairs.map(i => createCreateSignerOperation({
        publicKey: i.accountId(),
        roleID: '3',
        weight: '1000',
        identity: '0',
      }))

      await api.postOperations(...ops)
    } catch (error) {
      console.log(error)
    }
  }
}

function createCreateSignerOperation (opts) {
  return base.ManageSignerBuilder.createSigner({
    publicKey: opts.publicKey,
    roleID: opts.roleID,
    weight: opts.weight,
    identity: opts.identity,
    details: {},
  })
}

addSigner()
