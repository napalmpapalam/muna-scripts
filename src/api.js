const { ApiCaller, WalletsManager, base, Wallet, FactorsManager, BLOB_TYPES } = require('@tokend/js-sdk')

const geocode = { latitude: 0, longitude: 0 }
const api = new ApiCaller()
const walletsManager = new WalletsManager()
const factorsManager = new FactorsManager()

const ENVIROMENTS = {
  local: 'local',
  staging: 'staging',
  stagingKeemoji: 'stagingKeemoji',
  stagingAura: 'stagingAura',
  prod: 'prod',
  prodAura: 'prodAura',
}

const HORIZON_ENV = {
  [ENVIROMENTS.local]: 'http://localhost:8000/_/api',
  [ENVIROMENTS.staging]: 'https://api.staging.munahealth.com',
  [ENVIROMENTS.stagingAura]: 'https://api.stage.aura.art',
  [ENVIROMENTS.stagingKeemoji]: 'https://api.keemoji.tokend.io',
  [ENVIROMENTS.prod]: 'https://api.munahealth.com',
  [ENVIROMENTS.prodAura]: 'https://api.beta.aura.art',
}

const isEnvExist = value => Object.keys(HORIZON_ENV).includes(value)

module.exports = {
  initApiWithWallet: async (opts) => {
    if (!opts.email || !opts.password || !isEnvExist(opts.env)) return

    const { email, password, env } = opts
    try {
      api.useBaseURL(HORIZON_ENV[env])
      walletsManager.useApi(api)
      const wallet = await walletsManager.get(email, password, geocode)

      api.useWallet(wallet)
      walletsManager.useApi(api)
      factorsManager.useApi(api)
      const { data: networkDetails } = await api.getRaw('/')
      api.useNetworkDetails(networkDetails)

      return wallet
    } catch (e) { console.log('initApiWithWallet ERROR: ', e) }
  },

  initApiWithMasterWallet: async (opts) => {
    if (!opts.accountId || !opts.seed || !isEnvExist(opts.env)) return

    const { accountId, seed: decryptedSeed, env } = opts

    try {
      api.useBaseURL(HORIZON_ENV[env])

      const keyPair = base.Keypair.fromSecret(decryptedSeed)
      const seed = keyPair.secret()
      const wallet = new Wallet('', seed, accountId)

      api.useWallet(wallet)
      walletsManager.useApi(api)
      factorsManager.useApi(api)
      const { data: networkDetails } = await api.getRaw('/')
      api.useNetworkDetails(networkDetails)

      return wallet
    } catch (e) { console.log('initApiWithMasterWallet ERROR:', e) }
  },

  createBlob: async (ownerAccontId, blobValue, blobType) => {
    try {
      const { data: blob } = await api.postWithSignature('/blobs', {
        data: {
          type: blobType,
          attributes: { value: JSON.stringify(blobValue) },
          relationships: {
            owner: { data: { id: ownerAccontId } },
          },
        },
      })

      console.log('GOT BLOB:', blob)
      return blob.id
    } catch (error) {
      console.log(error)
    }
  },

  createKycOperation: ({ kycBlobId, destinationAccountId, accountRole, requestID = '0' }) => {
    return base.CreateChangeRoleRequestBuilder.createChangeRoleRequest({
      requestID: `${requestID}`, // KYC_CREATION_REQUEST_ID
      destinationAccount: destinationAccountId,
      accountRoleToSet: `${accountRole}`,
      creatorDetails: { blob_id: kycBlobId },
      allTasks: 0,
    })
  },

  api,

  base,

  walletsManager,

  factorsManager,

  Wallet,

  BLOB_TYPES,

  ENVIROMENTS,
}
